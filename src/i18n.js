import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

import commonDE from './lang/common-de.json';
import commonEN from './lang/common-en.json';

const resources = {
  en: {
    common: commonEN,
  },
  de: {
    common: commonDE,
  },
};

const languageDetector = {
  type: 'languageDetector',
  init: () => {},
  detect: () => {
    const lng = localStorage.getItem('lng');

    return lng || 'de';
  },
  cacheUserLanguage: (lng) => {
    localStorage.setItem('lng', lng);
  },
};

i18n
  .use(languageDetector)
  .use(initReactI18next)
  .init({
    resources,
    defaultNS: 'common',
    fallbackLng: 'en',
    interpolation: {
      escapeValue: false,
    },
  });

export default i18n;
