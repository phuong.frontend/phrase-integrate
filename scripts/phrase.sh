#!/bin/bash

local_path="$(pwd)"
source_path="$local_path/src/lang"

function commit {
  git config credential.helper store
  
  echo "https://$BITBUCKET_USER:$BITBUCKET_PASSWORD@$BITBUCKET_URL" > ~/.git-credentials

  git remote add upstream "https://$BITBUCKET_URL"

  git fetch

  git checkout -b $BRANCH_NAME develop

  if git diff src/lang/common-en.json | grep -q "";
  then
    echo "sync translation from phrase"
    git add src/lang/
    git commit -m "update translation at $CI_JOB_STARTED_AT" --author "$CI_COMMIT_AUTHOR"
    git push upstream $BRANCH_NAME
  fi
}

function download_locale {
  arguments=($@)

  locale_id=${arguments[0]}
  locale_filename=${arguments[1]}

  curl "https://api.phrase.com/v2/projects/$PHRASE_PROJECT_ID/locales/$locale_id/download?file_format=i18next" \
    -H "Authorization: token $PHRASE_TOKEN" \
    -o "$source_path/$locale_filename" \
    -s
  
  # check if locale is valid
  if grep -q "documentation_url" "$source_path/$locale_filename"
  then
    echo "$locale_filename is not valid"

    # restore 
    echo "restore to the local $locale_filename"
    git checkout "$source_path/$locale_filename"
  else
    echo "$locale_filename is valid"
  fi
}

function upload_primary_locale {
  echo "============== Start upload EN locale =============="
  response=$(curl -i -s "https://api.phrase.com/v2/projects/$PHRASE_PROJECT_ID/uploads" \
    -X POST \
    -H "Authorization: token $PHRASE_TOKEN" \
    -F "file=@$source_path/common-en.json" \
    -F file_format=i18next \
    -F "locale_id=$PHRASE_ENGLISH_ID")

  echo $response
}

function download_locales_from_phrase {
  echo "============== Start download EN =============="
  download_locale $PHRASE_ENGLISH_ID common-en.json

  echo "============== Start download DE =============="
  download_locale $PHRASE_GERMAN_ID common-de.json
}
